packages=(
	'xorg-server'
	'xorg-xinit'
	'xorg-xsetroot'
	'i3-gaps'
	'rofi'
	'ttf-cascadia-code'
	'noto-fonts'
	'thunar'
	'gvfs'
	'gvfs-mtp'
	'mpv'
	'ristretto'
	'file-roller'
	'p7zip'
	'unrar'
	'unzip'
	'zip'
	'lxappearance'
	'firefox'
	'clang'
	'cmake'
	'git'
	'lazygit'
	'gcc'
	'glibc'
	'nodejs'
	'npm'
	'python'
	'yarn'
	'python-pip'
)
for package in "${packages[@]}"; do
	clear &
	echo "installing ${package}" &
	sudo pacman -S "${package}" --noconfirm --needed &
done
